libconfig-model-openssh-perl (2.9.8.1-1) unstable; urgency=medium

  [ Dominique Dumont ]
  * new upstream version for OpenSsh 9.8
  * README.source.org: add missing instruction to update package
  * control: remove obsolete versioned dependencies
  * control: declare compliance with policy 4.7.0

 -- Dominique Dumont <dod@debian.org>  Tue, 10 Sep 2024 18:28:23 +0200

libconfig-model-openssh-perl (2.9.4.1-1) unstable; urgency=medium

  [ Dominique Dumont ]
  * new upstream version for OpenSsh 9.4
  * Now built with upstream git. pristine-tar branch update is no longer done.
  * control, rules: build with Dist::Zilla
  * add README.source.org
  * add gbp.conf and tests/pkg-perl/use-name
  * add patch to remove Git plugin calls from dist.ini
  * add fill.copyright.blanks

 -- Dominique Dumont <dod@debian.org>  Sat, 14 Oct 2023 19:20:23 +0200

libconfig-model-openssh-perl (2.9.0.2-1) unstable; urgency=medium

  * New upstream version 2.9.0.2
    * remove obsolete "backend" constructor parameter
      (Closes: #1016295)
  * control: declare compliance with policy 4.6.1

 -- Dominique Dumont <dod@debian.org>  Sun, 07 Aug 2022 18:11:22 +0200

libconfig-model-openssh-perl (2.9.0.1-1) unstable; urgency=medium

  * New upstream version 2.9.0.1
    * Add support for OpenSsh 9.0
  * copyright: updated year

 -- Dominique Dumont <dod@debian.org>  Sat, 28 May 2022 11:18:50 +0200

libconfig-model-openssh-perl (2.8.7.1-1) unstable; urgency=medium

  * New upstream version 2.8.7.1
    * Support for OpenSsh 8.7
    * Added migration instructions for some parameters deprecated
      between OpenSsh 8.4 and 8.7
  * control: declare compliance with policy 4.6.0
  * control: build-dep on libconfig-model-perl (>= 2.146)
  * delete patch fix-man-page

 -- Dominique Dumont <dod@debian.org>  Mon, 27 Dec 2021 12:58:28 +0100

libconfig-model-openssh-perl (2.8.4.3-2) unstable; urgency=medium

  * re-released to unstable

 -- Dominique Dumont <dod@debian.org>  Mon, 06 Sep 2021 17:57:57 +0200

libconfig-model-openssh-perl (2.8.4.3-1) experimental; urgency=medium

  * Import upstream version 2.8.4.3.
    * Keep order of Host and Match sections (fix gh #6)
    * Fix crash when starting with empty user file (fix gh #5)
  * add patch to fix man page format

 -- Dominique Dumont <dod@debian.org>  Sun, 09 May 2021 16:24:04 +0200

libconfig-model-openssh-perl (2.8.4.2-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Update standards version to 4.4.1, no changes needed.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.
  * Import upstream version 2.8.4.2.
  * Update years of upstream and packaging copyright.
  * Update debian/upstream/metadata.
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Sun, 17 Jan 2021 14:57:01 +0100

libconfig-model-openssh-perl (2.8.0.1-1) unstable; urgency=medium

  * New upstream version 2.8.0.1
     * Add support for OpenSsh 8.0
     * New version number scheme: v<major>.<OpenSsh version>.<minor>.
    Under the hood, the major change of this release is the way Ssh and
    Sshd model are updated. It used to be a manual work (which explains
    why only OpenSsh 6.4 parameters were supported). Now ssh models are
    generated from ssh_config and sshd_config manuel pages. All
    parameters from OpenSsh 8.0 are supported and obsolete parameters
    are silently dropped or migrated to new parameters.
  * control:
    * depends on libconfig-model-perl >= 2.134
    * depends on libconfig-model-tester-perl >= 4.001
    * declare compliance with policy 4.4.0
    * add BD-I on libtest-pod-perl
  * use new compat dependency (cme)
  * remove add-debian-param patch (supported upstream)
  * removed obsolete demo files (removed upstream)

 -- Dominique Dumont <dod@debian.org>  Mon, 09 Sep 2019 14:03:05 +0200

libconfig-model-openssh-perl (1.241-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Dominique Dumont ]
  * New upstream version 1.241
    * Fix behavior of 'cme -force' so user can load ssh config files
      containing errors.
    * Model update: PortForward parameter: port must be alphanumeric
  * control: depends on libconfig-model-perl >= 2.128
  * control: declare compliance with version 4.2.1

 -- Dominique Dumont <dod@debian.org>  Sun, 09 Dec 2018 19:41:00 +0100

libconfig-model-openssh-perl (1.239-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Dominique Dumont ]
  * New upstream version 1.239
    * OpenSsh backend: fix backend (broke with Config::Model 2.123,
      Sorry about the mess).
    * added t/README.md
    * remove deprecated suffix method
  * control:
    * declare comliance with Policy 4.1.4
    * depends on libconfig-model-perl >= 2.123
    * BD-I on libconfig-model-tester-perl >= 3.006
    *  update Vcs-Browser and Vcs-Git
  * remove test-failure.patch (obsolete)

 -- Dominique Dumont <dod@debian.org>  Wed, 09 May 2018 10:10:38 +0200

libconfig-model-openssh-perl (1.238-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Jotam Jr. Trejo from Uploaders. Thanks for your work!

  [ Dominique Dumont ]
  * New upstream version 1.238
    * update models to use new rw_config parameter
    * update Ciphers parameter
    * UseLogin parameter is deprecated
  * control: depends on libconfig-model-perl >= 2.111
  * control: declare compliance with policy 4.1.1

 -- Dominique Dumont <dod@debian.org>  Tue, 10 Oct 2017 12:41:34 +0200

libconfig-model-openssh-perl (1.237-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Dominique Dumont ]
  * Imported Upstream version 1.237
  * control:
    - removed build dep on perl-tk
    + added depends on cme
    * updated std-version to 3.9.7
    * minor description update
  * copyright: updated years

 -- Dominique Dumont <dod@debian.org>  Mon, 07 Mar 2016 19:59:42 +0100

libconfig-model-openssh-perl (1.236-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Declare compliance with Debian Policy 3.9.6.
  * Add build dependency on libmodule-build-perl.
  * Mark package as autopkgtest-able.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Fri, 05 Jun 2015 15:26:47 +0200

libconfig-model-openssh-perl (1.236-1) unstable; urgency=medium

  * Imported Upstream version 1.236
    * removed experience parameters from OpenSsh model
    * warn and propose a fix when public key is used as
      IdendityFile
  - control: removed BD on libanyevent and libev-perl

 -- Dominique Dumont <dod@debian.org>  Fri, 23 May 2014 13:28:37 +0200

libconfig-model-openssh-perl (1.235-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Dominique Dumont ]
  * Imported Upstream version 1.235:
    * Ssh backends: send a clear error message when unknown
      parameters are found
    * Ssh: added deprecated UseRSh and FallBackToRsh
    * Ssh::GSSAPI* params: set upstream_default to 0 (instead of default)
    * fixed typo in Sshd::MatchCondition  description (tx gregoa)
  * control: depends on libconfig-model-perl 2.050

 -- Dominique Dumont <dod@debian.org>  Sat, 05 Apr 2014 11:56:16 +0200

libconfig-model-openssh-perl (1.232-1) unstable; urgency=medium

  * Imported Upstream version 1.232
    + Added parameters supported by OpenSsh 6.4
    * fixed typo in Sshd::MatchCondition description (tx gregoa)
  * compat: bumped to 9
  * control: bumped std-version to 3.9.5
  * control: refreshed dependencies
  + added patch to add support for sshd_config Debian specific parameter
  - removed spelling.patch (applied upstream)

 -- Dominique Dumont <dod@debian.org>  Sun, 29 Dec 2013 17:07:17 +0100

libconfig-model-openssh-perl (1.230-1) unstable; urgency=low

  * New upstream release 1.228.
  * Update (build) dependencies.
  * Update years of packaging copyright and license stanza.
  * Drop patch, applied upstream.
  * Add debian/NEWS with interesting interface changes.
  * Build depend on Config::Model::Tester. (Closes: #720788)
  * Add patch test-failure.patch. Fix test failure caused by undefined
    variable.
  * Add patch to fix a spelling mistake.

  * New upstream release 1.230.
  * Update (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sat, 31 Aug 2013 15:19:58 +0200

libconfig-model-openssh-perl (1.227-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Dominique Dumont ]
  * Imported Upstream version 1.227:
    * Removed Augeas backend (no longer needed, comments are handled
      by Config::Model::Backend::OpenSsh)
    * Replaced Any::Moose with Mouse.
  * control:
    * updated std-version
    * added enhances openssh-client openssh-server
    * updated description
    * depends on libmouse-perl instead of libany-moose-perl
    * depends on libconfig-model-perl ( >= 2.034)
  * copyright: updated years

 -- Dominique Dumont <dod@debian.org>  Sat, 04 May 2013 21:11:02 +0200

libconfig-model-openssh-perl (1.225-1) unstable; urgency=low

  * New upstream version (Doc and demo fix release)
    * updated demos to use cme instead of deprecated config-edit
    * likewise, clean up pod doc to use cme command
    * removed non utf-8 char from ssh doc (Fix RT 79077)
  * removed README and TODO: no new information compared to man pages

 -- Dominique Dumont <dod@debian.org>  Mon, 29 Oct 2012 13:47:07 +0100

libconfig-model-openssh-perl (1.224-1) unstable; urgency=low

  * new upstream release
    * Backend: make sure that AuthorizedKeysFile items are
      written on a single line.

 -- Dominique Dumont <dod@debian.org>  Tue, 22 May 2012 16:27:03 +0200

libconfig-model-openssh-perl (1.222-1) unstable; urgency=low

  * new upstream version:
    * added AuthorizedKeysFile2 parameter (See Debian #671367) and migration
      from AuthorizedKeysFile2 to AuthorizedKeysFile to help migration
      from Debian Squeeze to Wheezy
    * replaced deprecated get_all_indexes with fetch_all_indexes
  * control:
    * depends on Config::Model 2.015
    + added build dependencies required by usage of Config::Model::Tester

 -- Dominique Dumont <dod@debian.org>  Thu, 17 May 2012 16:53:19 +0200

libconfig-model-openssh-perl (1.221-1) unstable; urgency=low

  * New Upstream version:
    * Ssh model: ControlMaster also supports auto keyword (thanks to
      harleypig and Daniel Dehennin, Closes: #670319)
  * control: updated standard-version (no other changes)
  * copyright: updated format url

 -- Dominique Dumont <dod@debian.org>  Mon, 30 Apr 2012 09:53:13 +0200

libconfig-model-openssh-perl (1.220-1) unstable; urgency=low

  * Imported Upstream version 1.220:
    * Fix FTBS test (Closes: #660371)
  * control: BDI on libconfig-model-perl >= 2.004 (since only the tests
    use the new method call, a similar BD is not required)
  * copyright: updated (c) years and my email address
  * source compat: bumped to 8 (no other changes)

 -- Dominique Dumont <dod@debian.org>  Tue, 21 Feb 2012 11:58:22 +0100

libconfig-model-openssh-perl (1.219-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Dominique Dumont ]
  * Imported Upstream version 1.219
    * Ssh model: do not warp LocalForward with GatewayPorts.
      They are independent
    * Ssh backend: store root config in layered data instead of
      preset data (also fix RT#72916)
  * control: depends on libconfig-model-perl 1.265

 -- Dominique Dumont <dod@debian.org>  Wed, 07 Dec 2011 14:30:00 +0100

libconfig-model-openssh-perl (1.218-1) unstable; urgency=low

  * New upstream release: Fix bug that tried to open a file in /etc
    when saving ssh config as a regular user.
  * control: updated my email address

 -- Dominique Dumont <dod@debian.org>  Sat, 23 Jul 2011 19:12:59 +0200

libconfig-model-openssh-perl (1.217-1) unstable; urgency=low

  * New upstream release
    - fix bug to avoid keyword without value lines
  * control: added BDI on libtest-differences-perl

 -- Dominique Dumont <domi.dumont@free.fr>  Tue, 17 May 2011 14:33:24 +0200

libconfig-model-openssh-perl (1.216-1) unstable; urgency=low

  [ Jotam Jr. Trejo ]
  * New upstream release 1.215
  * debian/control: update Config::Model dependency to require
    1.236 version
  * debian/control: bump Standards Version to 3.9.2 no changes needed
  * Add  myself to Uploaders and Copyright

  [ gregor herrmann ]
  * New upstream release 1.216.
  * Install scripts in demo/ as examples instead of as docs.
  * debian/control: explicitly (build) depend on libany-moose-perl.
  * Add a patch to fix a tiny spelling mistake.

 -- gregor herrmann <gregoa@debian.org>  Mon, 11 Apr 2011 19:37:55 +0200

libconfig-model-openssh-perl (1.214-1) unstable; urgency=low

  * New upstream release
    - fixed missing files used for auto-completion
    - Fixed Ssh backend to write Host pattern annotations/comments
  * control: updated description

 -- Dominique Dumont <domi.dumont@free.fr>  Thu, 03 Mar 2011 14:54:03 +0100

libconfig-model-openssh-perl (1.213-1) unstable; urgency=low

  * New upstream release:
    - *.t: fixed tests (Closes: #605792)
    - removed config-edit-*. config-edit now has auto-completion and
      can be invoked with '-application ssh' or '-application sshd'
    - Single backend was split in 3 (OpenSsh, Ssh and Sshd) to benefit
      from C::M::Backend::Any
  * control: removed dependency on Parse::RecDescent, depend
    on Config::Model 1.234
  * copyright: updated format and copyright years
  * rules: back to basics since config-edit-sshd is gone
  * removed libconfig-model-openssh-perl.dirs
  * added debian/source/format
  * added demo in docs file

 -- Dominique Dumont <domi.dumont@free.fr>  Mon, 28 Feb 2011 19:19:37 +0100

libconfig-model-openssh-perl (1.210-1) unstable; urgency=low

  * New upstream release

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 24 Jan 2010 21:23:09 -0500

libconfig-model-openssh-perl (1.209-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Rewrite control description
  * Update debhelper rules to use $(TMP) shortcut
  * Drop package dependencies satisfied by oldstable
  * Update copyright to new DEP5 format
  * Add myself to Uploaders and Copyright

  [ gregor herrmann ]
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.
  * debian/copyright: update years of upstream copyright.

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 18 Jan 2010 18:28:27 -0500

libconfig-model-openssh-perl (1.208-1) unstable; urgency=low

  [ Dominique Dumont ]
  * New upstream release (minor model changes for Tk config wizard)
  * control: updated Standard-Version to 3.8.3

  [ gregor herrmann ]
  * Minimize debian/rules.

 -- Dominique Dumont <dominique.dumont@hp.com>  Fri, 11 Sep 2009 13:18:29 +0200

libconfig-model-openssh-perl (1.207-1) unstable; urgency=low

  * New upstream release (fix model error)

  * Fix "test suite fails on amd64" (upstream skips tests that cannot
    work when run as root)
    (Closes: #537395)

 -- Dominique Dumont <dominique.dumont@hp.com>  Wed, 29 Jul 2009 13:23:32 +0200

libconfig-model-openssh-perl (1.206-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Dominique Dumont ]
  * New upstream release
  * control: depends on libconfig-model-perl 0.637
  * control: depends on libparse-recdescent
  * control: updated to policy 3.8.2

 -- Dominique Dumont <dominique.dumont@hp.com>  Fri, 26 Jun 2009 17:02:31 +0200

libconfig-model-openssh-perl (1.205-2) unstable; urgency=low

  * (Build) depend on libconfig-model-backend-augeas-perl >= 0.107
    (closes: #531233).

 -- gregor herrmann <gregoa@debian.org>  Mon, 01 Jun 2009 20:32:31 +0200

libconfig-model-openssh-perl (1.205-1) unstable; urgency=low

  * New upstream release:
    - small bug fix that breaks with libconfig-model-perl 0.635
  * control: change Standards-Version: 3.8.1

 -- Dominique Dumont <dominique.dumont@hp.com>  Sun, 19 Apr 2009 14:46:00 +0200

libconfig-model-openssh-perl (1.204-1) unstable; urgency=low

  * New upstream release.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Tue, 10 Mar 2009 20:57:03 +0100

libconfig-model-openssh-perl (1.203-1) unstable; urgency=low

  * Initial Release. (Closes: #514013)

 -- Dominique Dumont <dominique.dumont@hp.com>  Tue, 3 Feb 2009 13:54:07 +0100
